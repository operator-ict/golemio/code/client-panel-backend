const { validationResult, param } = require("express-validator/check");
const { matchedData } = require("express-validator/filter");
const errorMessages = require("../error/messages");
const PostgresConnector = require("../../dataAccess/postgresConnector");

module.exports.options = {};

module.exports.checkErrors = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.mapped() });
    }
    req.body = matchedData(req, { locations: ["body"] });
    if (req.session && req.session.user && req.session.user.id) {
        if (req.method === "PUT") {
            req.body.updated_by_user_id = req.session.user.id;
        } else if (req.method === "POST") {
            req.body.created_by_user_id = req.session.user.id;
        }
    }
    next();
};

module.exports.checkId = (paramName, zeroEnabled) => {
    if (!Array.isArray(paramName)) {
        paramName = [paramName];
    }
    return (req, res, next) =>
        Promise.each(
            paramName,
            (item) =>
                new Promise((resolve, reject) =>
                    param(item, errorMessages.not_an_id).isInt({ min: zeroEnabled === true ? 0 : 1 })(req, res, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        resolve();
                    })
                )
        ).then(() => next());
};

module.exports.hasValidTagIds = async (tagIds) => {
    const tagModel = PostgresConnector.getModel("tag");
    //const count = await tagModel.count({ _id: { $in: tagIds } });
    const count = await tagModel.count({ where: { id: tagIds } });
    if (count !== tagIds.length) {
        throw new Error();
    }
};

module.exports.hasValidMetadataId = async (metadataId) => {
    //    const metadataModel = modelManager.getModel("metadata");
    const metadataModel = PostgresConnector.getModel("metadata");
    try {
        //const result = await metadataModel.findOne({ _id: metadataId }).select("_id").lean();
        const dataResult = await metadataModel.findOne({ where: { id: metadataId } });
        const result = dataResult.getDataValue("id"); //TODO
        if (!result) {
            throw new Error();
        }
    } catch (err) {
        throw err;
    }
};
