const rp = require('request-promise');
const jwt = require('jsonwebtoken');

class GolemioAuth {
    constructor(config, logger) {
        this._config = config;
        this._logger = logger;
    }

    verify(token) {
        const user = jwt.decode(token);
        return rp({
            url: `${this._config.authUrl}/user/${user.id}/route`,
            json: true,
            headers: {
                'x-access-token': token
            }
        })
            .then((routes) => ({
                ...user,
                routes
            }))
    }
}

module.exports = GolemioAuth;
