const users = [
    {
        email: 'user1@user.cz',
        token: 'token1',
        id: 1,
        admin: false,
        routes: [
            {
                "user_id": 2,
                "endpoint": "/traffic-cameras",
                "method": "GET"
            },
            {
                "user_id": 2,
                "endpoint": "/lig",
                "method": "GET"
            },
            {
                "user_id": 2,
                "endpoint": "/dashboard/1",
                "method": "GET"
            }
        ]
    },
    {
        email: 'user2@user.cz',
        token: 'token2',
        id: 2,
        admin: true,
        routes: [
            {
                "user_id": 2,
                "endpoint": "/lights",
                "method": "GET"
            },
            {
                "user_id": 2,
                "endpoint": "/parkings",
                "method": "GET"
            },
            {
                "user_id": 2,
                "endpoint": "/analyses/contractsmhmp/public-tenders",
                "method": "GET"
            },
            {
                "user_id": 2,
                "endpoint": "/v2/sortedwastestations",
                "method": "GET"
            },
        ]
    },
    {
        email: 'user3@user.cz',
        token: 'token3',
        id: 3,
        admin: true,
        routes: []
    }
];

class DummyAuth {
    constructor(config, logger) {
        this._config = config;
        this._logger = logger;
    }

    verify(token) {
        const user = users.filter(user => user.token === token);
        return Promise.resolve(user.length ? user[0] : null);
    }
}

module.exports = DummyAuth;
