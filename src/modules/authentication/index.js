const DummyStrategy = require("./strategy/dummy");
const GolemioStrategy = require("./strategy/golemio");

class Authentication {
    constructor(config, logger) {
        this.strategies = {
            dummy: DummyStrategy,
            golemio: GolemioStrategy,
        };

        if (!this.strategies[config.strategy]) {
            throw new Error("Unknown login strategy");
        }

        this._logger = logger;
        this._config = config;
        this._strategy = new this.strategies[config.strategy](config, logger);
    }

    verify(token) {
        return this._strategy.verify(token);
    }
}

module.exports = Authentication;
