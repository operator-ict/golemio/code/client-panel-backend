const nodemailer = require("nodemailer");
const { configLoader } = require("../../resources/accessrequest/configLoader");
const getLogger = require("../logger").getLogger;

async function sendFormMail(htmlMsg) {
    if (configLoader.mailer_enabled) {
        mailConfig = configLoader.mailer_transport;
    } else {
        // all emails are catched by ethereal.email
        const testAccount = await nodemailer.createTestAccount();
        mailConfig = {
            auth: {
                pass: testAccount.pass, // generated ethereal password
                user: testAccount.user, // generated ethereal user
            },
            host: "smtp.ethereal.email",
            port: 587,
            secure: false, // true for 465, false for other ports
        };
    }

    let transporter = nodemailer.createTransport(mailConfig);

    let mailOptions = {
        from: configLoader.mailer_from || '"Golemio Client Panel" <no-reply@operatorict.cz>',
        to: configLoader.mailer_to || "golemio@operatorict.cz",
        subject: "Golemio - nová zpráva od uživatele",
        html: htmlMsg,
    };

    return new Promise((resolve, reject) => {
        //If callback argument is not set then the method returns a Promise object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) reject(error);
            else {
                if (!configLoader.mailer_enabled)
                    getLogger("sendFormMail").info("Preview URL: " + nodemailer.getTestMessageUrl(info));
                resolve(info);
            }
        });
    });
}

module.exports = {
    sendFormMail,
};
