class Policy {

    init(config, authManager, logger, Error) {
        this._authManager = authManager;
        this._logger = logger;
        this._config = config;
        this._Error = Error;
        this._errorMessages = Error.messages;

        this.isAdmin = this.isAdmin.bind(this);
        this.isSignedIn = this.isSignedIn.bind(this);
    }

    isSignedIn(req, res, next) {
        const access_token = req.headers[this._config.token_name] || false;
        if (!access_token) {
            return next(new this._Error(this._errorMessages.no_access_token, 400));
        }
        return this._authManager
            .verify(access_token)
            .then((user) => {
                if (!user) {
                    return next(new this._Error(this._errorMessages.unauthorized, 401));
                }
                req.session = {
                    user
                };

                return next();
            })
            .catch((err) => {
                this._logger.error(err);
                return next(new this._Error(this._errorMessages.unauthorized, 401));
            });
    }
    isAdmin(req, res, next) {
        if (!req.session.user.admin) {
            return next(new this._Error(this._errorMessages.forbidden, 403));
        }

        return next();
    }
}

module.exports = new Policy();
