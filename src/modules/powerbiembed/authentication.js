// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------
const { PublicClientApplication, ConfidentialClientApplication, LogLevel } = require('@azure/msal-node');
const { config } = require('./utils');
const getLogger = require('../logger').getLogger;
const log = getLogger('powerbiembed:authentication');

const publicAccessToken = async () => {
    const msalConfig = {
        auth: {
            clientId: config.clientId,
            authority: `${config.authorityUri}/${config.tenantId}`
        },
        system: {
            loggerOptions: {
                loggerCallback(loglevel, message, containsPii) {
                    log.debug(message);
                },
                logLevel: LogLevel.Trace,
                piiLoggingEnabled: true,
            }
        }
    };

    const pca = new PublicClientApplication(msalConfig);

    const loginRequest = {
        scopes: [`${config.scope}/.default`],
        username: config.pbiUsername,
        password: config.pbiPassword
    };

    try {
        const start = performance.now();
        const authResult = await pca.acquireTokenByUsernamePassword(loginRequest);
        const end = performance.now();
        log.debug(`Time taken to acquire token: ${end - start}ms`);
        return {
            accessToken: authResult.accessToken,
            expiresOn: authResult.expiresOn
        };
    } catch (err) {
        throw err;
    }
}

const confidentialAccessToken = async () => {
    const msalConfig = {
        auth: {
            clientId: config.clientId,
            clientSecret: config.clientSecret,
            authority: `${config.authorityUri}/${config.tenantId}`
        },
    }

    const cca = new ConfidentialClientApplication(msalConfig);

    const authRequest = {
        scopes: [`${config.scope}/.default`]
    };

    try {
        const authResult = await cca.acquireTokenByClientCredential(authRequest);
        return {
            accessToken: authResult.accessToken,
            expiresOn: authResult.expiresOn
        };
    } catch (err) {
        throw err;
    }
}

async function getAccessToken() {
    if (config.authenticationMode.toLowerCase() === 'masteruser') {
        return await publicAccessToken();
    }

    if (config.authenticationMode.toLowerCase() === 'serviceprincipal') {
        return await confidentialAccessToken();
    }

    throw new Error(`Invalid authentication mode: ${config.authenticationMode}`);
}

module.exports.getAccessToken = getAccessToken;
