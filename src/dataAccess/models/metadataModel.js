const { DataTypes } = require("sequelize");

const MetadataAttributeModel = {
    id: { type: DataTypes.UUID, primaryKey: true, default: true },
    type: { type: DataTypes.STRING },
    component: { type: DataTypes.STRING },
    route: { type: DataTypes.STRING },
    client_route: { type: DataTypes.STRING },
    fa_icon: { type: DataTypes.STRING },
    title: { type: DataTypes.STRING },
    description: { type: DataTypes.STRING },
    data: { type: DataTypes.JSON },
    thumbnail_url: { type: DataTypes.STRING },
    thumbnail_s3_key: { type: DataTypes.STRING },
    thumbnail_content_type: { type: DataTypes.STRING },
};

module.exports = MetadataAttributeModel;
