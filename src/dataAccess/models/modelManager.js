const AttachmentAttributeModel = require("./attachment");
const FavouriteTilesAttributeModel = require("./favouriteTilesModel");
const MetadataAttributeModel = require("./metadataModel");
const MetadataTagAttributeModel = require("./metadataTagModel");
const TagAttributeModel = require("./tagModel");
const UserAttributeModel = require("./userModel");
const LastViewedAttributeModel = require("./lastViewedModel");

initiateModels = (sequelizeConn) => {
    const metadata = sequelizeConn.define("metadata", MetadataAttributeModel);
    const user = sequelizeConn.define("user", UserAttributeModel);
    const tag = sequelizeConn.define("tag", TagAttributeModel);
    const favouriteTiles = sequelizeConn.define("favourite-tiles", FavouriteTilesAttributeModel);
    const lastViews = sequelizeConn.define("last_views", LastViewedAttributeModel);
    const metadataTag = sequelizeConn.define("metadata-tag", MetadataTagAttributeModel);
    const attachments = sequelizeConn.define("attachments", AttachmentAttributeModel);

    associateMetadataTag(metadata, tag, metadataTag);
    associateFavouriteTiles(user, tag, favouriteTiles);
    associateAttachments(metadata, attachments);
    associateLastViewed(user, metadata, lastViews);
};

associateMetadataTag = (mainTable, secondaryTable, relationTable) => {
    mainTable.belongsToMany(secondaryTable, {
        through: relationTable,
        foreignKey: "metadata_id",
        otherKey: "tag_id",
        as: "tags",
    });
    secondaryTable.belongsToMany(mainTable, {
        through: relationTable,
        foreignKey: "tag_id",
        otherKey: "metadata_id",
    });
    relationTable.belongsTo(mainTable, {
        targetKey: "id",
        foreignKey: "metadata_id",
    });
    relationTable.belongsTo(secondaryTable, {
        targetKey: "id",
        foreignKey: "tag_id",
    });
    mainTable.hasMany(relationTable, {
        sourceKey: "id",
        foreignKey: "metadata_id",
        as: "metadataTags",
    });
    secondaryTable.hasMany(relationTable, {
        sourceKey: "id",
        foreignKey: "tag_id",
    });
};

associateFavouriteTiles = (mainTable, secondaryTable, relationTable) => {
    mainTable.belongsToMany(secondaryTable, {
        through: relationTable,
        foreignKey: "user_id",
        otherKey: "metadata_id",
    });
    secondaryTable.belongsToMany(mainTable, {
        through: relationTable,
        foreignKey: "metadata_id",
        otherKey: "user_id",
    });
    relationTable.belongsTo(mainTable, {
        targetKey: "id",
        foreignKey: "user_id",
    });
    relationTable.belongsTo(secondaryTable, {
        targetKey: "id",
        foreignKey: "metadata_id",
    });
    mainTable.hasMany(relationTable, {
        sourceKey: "id",
        foreignKey: "user_id",
    });
    secondaryTable.hasMany(relationTable, {
        sourceKey: "id",
        foreignKey: "metadata_id",
    });
};

associateAttachments = (mainTable, relationTable) => {
    mainTable.hasOne(relationTable, {
        sourceKey: "id",
        foreignKey: "metadata_id",
        as: "attachment"
    });
    relationTable.belongsTo(mainTable, {
        targetKey: "id",
        foreignKey: "metadata_id",
    });
};

associateLastViewed = (firstTable, secondTable, relationTable) => {
    relationTable.belongsTo(firstTable, {
        targetKey: "user_id",
        foreignKey: "user_id",
    });
    relationTable.belongsTo(secondTable, {
        targetKey: "id",
        foreignKey: "metadata_id",
    });
    firstTable.hasMany(relationTable, {
        sourceKey: "user_id",
        foreignKey: "user_id",
        as: "lastViews",
    });
    secondTable.hasMany(relationTable, {
        sourceKey: "id",
        foreignKey: "metadata_id",
        as: "lastViews",
    });
};

module.exports = initiateModels;
