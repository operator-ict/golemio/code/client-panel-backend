const { DataTypes } = require("sequelize");

const LastViewedAttributeModel =  {
    user_id: { type: DataTypes.NUMBER, primaryKey: true },
    metadata_id: { type: DataTypes.UUID, primaryKey: true },
    last_visit: { type: DataTypes.DATE },
    number_of_visits_normalized: { type: DataTypes.INTEGER }
};

module.exports = LastViewedAttributeModel;
