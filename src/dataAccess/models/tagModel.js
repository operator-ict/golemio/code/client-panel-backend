const { DataTypes } = require("sequelize");

const TagAttributeModel = {
    id: { type: DataTypes.UUID,  primaryKey: true },
    title: { type: DataTypes.STRING },
};

module.exports = TagAttributeModel;
