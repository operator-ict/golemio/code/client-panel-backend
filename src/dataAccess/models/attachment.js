const { DataTypes } = require("sequelize");

const AttachmentAttributeModel = {
    id: { type: DataTypes.UUID, autoIncrement: true, primaryKey: true },
    metadata_id: { type: DataTypes.UUID, unique: true },
    title: { type: DataTypes.STRING },
    name: { type: DataTypes.STRING },
    content_type: { type: DataTypes.STRING },
    size: { type: DataTypes.INTEGER },
    url: { type: DataTypes.STRING },
    s3_key: { type: DataTypes.STRING },
};

module.exports = AttachmentAttributeModel;
