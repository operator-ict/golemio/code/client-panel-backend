const { StorageServiceFactory, StorageProvider: Provider } = require("@golemio/core/dist/helpers/data-access/storage");

class StorageProvider {
    static getInstance(config, logger) {
        if (!this._instance) {
            this._instance = new StorageServiceFactory(config, logger).getService(Provider.Azure);
        }

        return this._instance;
    }
}

module.exports = StorageProvider;
