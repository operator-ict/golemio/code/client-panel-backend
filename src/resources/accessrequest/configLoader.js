const configLoader = {
    mailer_enabled: process.env.MAILER_ENABLED === "true" || false,
    mailer_from: process.env.MAILER_FROM || process.env.MAILER_USERNAME,
    mailer_to: process.env.MAILER_TO,
    mailer_transport: {
        auth: {
            pass: process.env.MAILER_PASSWORD,
            user: process.env.MAILER_USERNAME,
        },
        host: process.env.MAILER_HOST,
        port: process.env.MAILER_PORT,
    },

    storage: {
        enabled: process.env.STORAGE_ENABLED === "true",
        provider: {
            azure: {
                connectionString: process.env.AZURE_BLOB_STORAGE_CONN ?? "",
                containerName: process.env.AZURE_BLOB_STORAGE_CONTAINER_NAME ?? "",
            },
        },
        attachmentsPath: process.env.STORAGE_ATTACHMENTS_PATH ?? "",
        thumbnailsPath: process.env.STORAGE_THUMBNAILS_PATH ?? "",
        fileMaxSize: process.env.STORAGE_FILE_MAX_SIZE_IN_B ? +process.env.STORAGE_FILE_MAX_SIZE_IN_B : 5242880, // 5MB
    },
};

module.exports = { configLoader };
