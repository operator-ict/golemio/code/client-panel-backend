const PostgresConnector = require("../../dataAccess/postgresConnector");
const TagMapper = require("./helpers/mapper");

const detail = async (req, res, next) => {
    try {
        const listOfUserRoutes = req.session.user.routes.map((route) => route.endpoint);
        const userMetadata = await PostgresConnector.getModel("metadata").findAll({
            where: { route: listOfUserRoutes },
            include: "tags",
        });
        const flatTags = userMetadata.map((metadata) => metadata.tags).flat(1);
        const uniqueTags = [];
        const uniqueIds = new Map();

        for (const item of flatTags) {
            if (!uniqueIds.has(item.id)) {
                uniqueIds.set(item.id, true);
                uniqueTags.push(TagMapper.toDto(item));
            }
        }

        res.json(uniqueTags);
    } catch (err) {
        next(err);
    }
};

const adminDetail = (req, res, next) => {
    PostgresConnector.getModel("tag")
        .findOne({ where: { id: req.params.tagId } })
        .then((data) => (data && res.json(TagMapper.toDto(data))) || res.sendStatus(404))
        .catch(next);
};

const list = (req, res, next) => {
    PostgresConnector.getModel("tag")
        .findAll()
        .then((data) => res.json(data.map((item) => TagMapper.toDto(item))))
        .catch(next);
};

const create = async (req, res, next) => {
    const data = await PostgresConnector.getModel("tag").create(TagMapper.toDao(req.body));
    res.status(201).json(TagMapper.toDto(data));
};

const update = async (req, res, next) => {
    const tagId = req.params.tagId;
    const tagToUpdate = await PostgresConnector.getModel("tag").findOne({ where: { id: tagId } });

    if (!tagToUpdate) {
        return res.sendStatus(404);
    }

    const newTag = TagMapper.toDao(req.body);
    tagToUpdate.title = newTag.title;
    await tagToUpdate
        .save()
        .then(() => res.sendStatus(204))
        .catch(next);
};

const deleteTag = async (req, res, next) => {
    const tagId = req.params.tagId;
    const tagToDelete = await PostgresConnector.getModel("tag").findOne({ where: { id: tagId } });

    if (!tagToDelete) {
        return res.sendStatus(404);
    }

    await tagToDelete
        .destroy()
        .then(() => res.sendStatus(204))
        .catch(next);
};

module.exports = {
    detail,
    create,
    list,
    update,
    adminDetail,
    delete: deleteTag,
};
