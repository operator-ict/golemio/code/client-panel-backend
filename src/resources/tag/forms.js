const { body, param } = require('express-validator/check');
const errorMessages = require('../../modules/error/messages');

const create = [
  body('title').exists().withMessage(errorMessages.required),
];

const checkId = () => {
  return param('tagId')
    .exists().withMessage(errorMessages.required)
    .isUUID().withMessage(errorMessages.not_an_id);
}

const adminDetail = [
  checkId(),
];

const deleteGroup = [
  checkId(),
]

module.exports = {
  create,
  adminDetail,
  update: [
    ...create,
    checkId(),
  ],
  delete: deleteGroup,
};
