const { Router } = require('express');
const forms = require('./forms');
const controller = require('./controller');
const policy = require('../../modules/policy');
const APIError = require('../../modules/error');
const errorMessages = require('../../modules/error/messages');
const validation = require('../../modules/validation');
const checkErrors = require('../../modules/validation').checkErrors;

const router = new Router();
const baseUrl = '/tag';

router.get(
  `/user/:userId${baseUrl}`,
  policy.isSignedIn,
  validation.checkId('userId'),
  (req, res, next) => {
    if (req.session.user.admin || req.session.user.id === parseInt(req.params.userId, 10)) {
      return next();
    }
    return next(new APIError(errorMessages.forbidden, 403));
  },
  controller.detail
);

router.get(
  baseUrl,
  policy.isSignedIn,
  policy.isAdmin,
  controller.list
);

router.get(
  `${baseUrl}/:tagId`,
  policy.isSignedIn,
  policy.isAdmin,
  forms.adminDetail,
  checkErrors,
  controller.adminDetail
);

router.put(
  `${baseUrl}/:tagId`,
  policy.isSignedIn,
  policy.isAdmin,
  forms.update,
  checkErrors,
  controller.update
);

router.post(
  baseUrl,
  policy.isSignedIn,
  policy.isAdmin,
  forms.create,
  checkErrors,
  controller.create
);

router.delete(
  `${baseUrl}/:tagId`,
  policy.isSignedIn,
  policy.isAdmin,
  forms.delete,
  checkErrors,
  controller.delete
);

module.exports = router;
