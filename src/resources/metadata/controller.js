const Jimp = require("jimp");
const errorMessages = require("../../modules/error/messages");
const PostgresConnector = require("../../dataAccess/postgresConnector");
const pbiUtils = require("../../modules/powerbiembed/utils");
const pbiEmbedToken = require("../../modules/powerbiembed/embedConfigService");
const { getLogger } = require("nodemailer/lib/shared");
const storageService = require("../../dataAccess/storageService");
const MetadataMapper = require("./helpers/mapper");
const path = require("path");
const rp = require("request-promise");
const { literal, Op, col } = require("sequelize");

const DAYS_OLD_NORM_FACTOR = 180;

const list = (req, res, next) => {
    new Promise((resolve, reject) => {
        // user is admin and wants to show metadata of other user
        if (req.session.user.admin && req.session.user.id !== parseInt(req.params.userId, 10)) {
            const tokenHeader = process.env.TOKEN_NAME || "x-access-token"
            // admin user can view routes o all other user
            return rp.get({
                url: `${process.env.AUTH_URL}/user/${req.params.userId}/route`,
                json: true,
                headers: {
                    'x-access-token': req.headers[tokenHeader]
                }
            })
                .then(resolve)
                .catch(reject)
        }

        // user wants to show own metadata
        return resolve(req.session.user.routes);
    })
    .then((routes) => {
        return PostgresConnector.getModel("metadata")
            .findAll({
                attributes:{
                    include: [[literal(req.params.userId), "user_id"]]
                },
                where: { route: routes.map((route) => route.endpoint) },
                subQuery: true,
                include: [
                    "tags",
                    "attachment",
                    {
                        model: PostgresConnector.getModel("last_views"),
                        as: "lastViews",
                        on: {
                            metadata_id: {
                                [Op.eq]: col(`metadata.id`),
                            },
                            user_id: {
                                [Op.eq]: col(`metadata.user_id`),
                            },
                        },
                    }
                ],
                order: [
                    ...req.query.sortBy === "visits"
                        ? [[{ model: PostgresConnector.getModel("last_views"), as: "lastViews" }, "number_of_visits_normalized", "DESC NULLS LAST"]]
                        : [],
                    ["id", "ASC"]
                ],
            })
    })
    .then((metadata) => metadata.map((element) => MetadataMapper.toDto(element)))
    .then((data) => res.json(data))
    .catch(next);
};

const adminDetail = (req, res, next) => {
    PostgresConnector.getModel("metadata")
        .findOne({ where: { id: req.params.metadataId }, include: ["tags", "attachment"] })
        .then((data) => (data && res.json(MetadataMapper.toDto(data))) || res.sendStatus(404))
        .catch(next);
};

const adminList = (req, res, next) => {
    PostgresConnector.getModel("metadata")
        .findAll({
            attributes:{
                include: [[literal(req.session.user.id), "user_id"]]
            },
            subQuery: true,
            include: [
                "tags",
                "attachment",
                {
                    model: PostgresConnector.getModel("last_views"),
                    as: "lastViews",
                    required: false,
                    on: {
                        metadata_id: {
                            [Op.eq]: col(`metadata.id`),
                        },
                        user_id: {
                            [Op.eq]: col(`metadata.user_id`),
                        },
                    },
                }
            ],
            order: [
                ...req.query.sortBy === "visits"
                    ? [[{ model: PostgresConnector.getModel("last_views"), as: "lastViews" }, col("number_of_visits_normalized"), "DESC NULLS LAST"]]
                    : [],
                ["id", "ASC"]
            ],
        })
        .then((metadata) => metadata.map((element) => MetadataMapper.toDto(element)))
        .then((data) => res.json(data))
        .catch((err) => {
            getLogger("MetadataController").error(err);
            next(err);
        });
};

const create = (req, res, next) => {
    PostgresConnector.getModel("metadata")
        .create(MetadataMapper.toDao(req.body), { include: "metadataTags" })
        .then((data) => res.status(201).json(data))
        .catch(next);
};

const update = async (req, res, next) => {
    const metadataId = req.params.metadataId;
    try {
        const metadataToUpdate = await PostgresConnector.getModel("metadata").findOne({
            where: { id: metadataId },
            include: "metadataTags",
        });

        if (!metadataToUpdate) {
            return res.sendStatus(404);
        }

        const metadataDao = MetadataMapper.toDao(req.body);
        const t = await PostgresConnector.getConnection().transaction();

        try {
            // synchronize tags m:n relation
            if (metadataDao.metadataTags && metadataDao.metadataTags.length > 0) {
                const metadataTagModel = PostgresConnector.getModel("metadata-tag");
                const tagIds = metadataDao.metadataTags.map((element) => element.tag_id);
                const tagAssociations = await metadataTagModel.findAll({ where: { metadata_id: metadataId }, transaction: t });

                const tagsToDelete = tagAssociations.filter((element) => tagIds.indexOf(element.tag_id) == -1);
                tagsToDelete.forEach((element) => {
                    element.destroy({ transaction: t });
                });

                const tagsToCreate = tagIds.filter((element) => tagAssociations.map((x) => x.tag_id).indexOf(element) == -1);
                tagsToCreate.forEach((element) => {
                    metadataTagModel.create(
                        {
                            tag_id: element,
                            metadata_id: metadataId,
                        },
                        { transaction: t }
                    );
                });
            }

            await PostgresConnector.getModel("metadata").update(MetadataMapper.toDao(req.body), {
                where: { id: metadataId },
                transaction: t,
            });

            await t.commit();
        } catch (err) {
            await t.rollback();
            getLogger("MetadataController").error(err);
            throw err;
        }

        res.sendStatus(204);
    } catch (err) {
        next(err);
    }
};

const deleteMetadata = async (req, res, next) => {
    const metadataId = req.params.metadataId;
    const metadataToDelete = await PostgresConnector.getModel("metadata").findOne({
        where: { id: metadataId },
        include: "attachment",
    });

    if (!metadataToDelete) {
        return res.sendStatus(404);
    }

    try {
        if (metadataToDelete.attachment && metadataToDelete.attachment.id)
            await storageService.deleteObjectByKey(metadataToDelete.attachment.s3_key);

        await metadataToDelete.destroy();
        return res.sendStatus(204);
    } catch (err) {
        getLogger("MetadataController").error(err);
        next(err);
    }
};

const uploadThumbnail = async (req, res, next) => {
    // allowed image format ratios: 333 x 74 & 394 x 180
    const IMG_SIZES = {
        width: 333,
        height: 74
    }

    const metadataId = req.params.metadataId;
    const file = req.file;

    if (!file) {
        return res.status(400).json({ errors: { file: { location: "body", param: "file", msg: "err_required" } } });
    }

    if (["image/jpeg", "image/png"].indexOf(file.mimetype) === -1) {
        return res.status(400).json({
            errors: { file: { location: "body", param: "file", msg: errorMessages.invalid_mimetype } },
        });
    }

    try {
        const image = await Jimp.read(file.buffer);
        const w = image.bitmap.width;
        const h = image.bitmap.height;

        // check minimal image size
        if (w < IMG_SIZES.width || h < IMG_SIZES.height) {
            return res.status(400).json({
                errors: {
                    file: { location: "body", param: "file", msg: errorMessages.invalid_image_size },
                },
            });
        }

        const isOldFormatRatio = (IMG_SIZES.width * h) / IMG_SIZES.height === w;
        const isNewFormatRatio = Math.round((w / h) * 10) / 10 === 2.2;

        // check image ratio
        if (!isOldFormatRatio && !isNewFormatRatio) {
            return res.status(400).json({
                errors: {
                    file: { location: "body", param: "file", msg: errorMessages.invalid_image_ratio },
                },
            });
        }

        if (isOldFormatRatio) {
            image.resize(IMG_SIZES.width, Jimp.AUTO);
        }

        const data = await image.getBufferAsync(file.mimetype);
        const url = `/metadata/${metadataId}/thumbnail`;
        const suffix = path.extname(file.originalname);
        const storageKey = await storageService.uploadThumbnail(data, metadataId, suffix);

        await PostgresConnector.getModel("metadata").update(
            { thumbnail_url: url, thumbnail_s3_key: storageKey, thumbnail_content_type: file.mimetype },
            { where: { id: metadataId } }
        );

        return res.status(201).json({ url });
    } catch (err) {
        return next(err);
    }
};

const thumbnail = async (req, res, next) => {
    const metadataId = req.params.metadataId;
    try {
        const metadataDao = await PostgresConnector.getModel("metadata").findOne({ where: { id: metadataId } });
        const data = metadataDao.thumbnail_s3_key ? await storageService.getObjectAsStream(metadataDao.thumbnail_s3_key) : undefined;

        if (data) {
            res.setHeader("Content-Type", metadataDao.thumbnail_content_type);
            return data.pipe(res);
        } else {
            return res.sendStatus(404);
        }
    } catch (err) {
        return next(err);
    }
};

const deleteThumbnail = async (req, res, next) => {
    const metadataId = req.params.metadataId;
    try {
        const metadataDao = await PostgresConnector.getModel("metadata").findOne({ where: { id: metadataId } });
        await storageService.deleteObjectByKey(metadataDao.thumbnail_s3_key);
        metadataDao.thumbnail_content_type = null;
        metadataDao.thumbnail_s3_key = null;
        metadataDao.thumbnail_url = null;
        await metadataDao.save({ omitNull: false });

        return res.json({ isOk: true });
    } catch (err) {
        next(err);
    }
};

/**
 * Leaking bucket abstraction for tracking user dashboard visit activity.
 *
 * Normalizes 'number_of_visits' weight with regard to 'last_visit' decreasing at constant rate where
 * `DAYS_OLD_NORM_FACTOR` days old data are disregarded. Older the visit, less value it has.
 *
 */
const trackViews = async (req, res, next) => {
    const metadataId = req.params.metadataId;
    const userId = req.session.user.id

    await PostgresConnector.getModel("user").findOrCreate({
        where: { user_id: userId },
        default: {
            user_id: userId
        },
    });

    try {
        const tableName = PostgresConnector.getModel("last_views").name;
        await PostgresConnector.getConnection().query(
            `INSERT INTO ${tableName} ("user_id","metadata_id","last_visit","number_of_visits_normalized","created_at","updated_at")
             VALUES (${userId}, '${metadataId}', NOW(), 1, NOW(), NOW())
             ON CONFLICT ("user_id","metadata_id") DO UPDATE
             SET
             "last_visit"=EXCLUDED."last_visit",
             "number_of_visits_normalized"=GREATEST(
                ${tableName}."number_of_visits_normalized" -
                FLOOR(${tableName}."number_of_visits_normalized" * EXTRACT(day FROM DATE_TRUNC('day', NOW() - ${tableName}."last_visit"))) / ${DAYS_OLD_NORM_FACTOR}, 0
             ) + 1,
             "updated_at"=EXCLUDED."updated_at"
             RETURNING "user_id","metadata_id","last_visit","number_of_visits_normalized","created_at","updated_at";`
        );
        return res.json({ isOk: true });
    } catch (err) {
        next(err);
    }
};

const getPowerBIToken = async (req, res, next) => {
    const metadataId = req.params.metadataId;

    const metadata = await PostgresConnector.getModel("metadata").findOne({ where: { id: metadataId } });
    if (!metadata) {
        return res.status(404);
    }
    if (req.session.user.routes.map((r) => r.endpoint).indexOf(metadata.route) === -1) {
        return res.status(403).json(errorMessages.forbidden);
    }

    // get workspaceId and reportId from metadata
    const workspaceId = metadata.data ? metadata.data.workspace_id || metadata.data.get("workspace_id") : null;
    const reportId = metadata.data ? metadata.data.report_id || metadata.data.get("report_id") : null;

    // Validate whether all the required configurations are provided in config.json
    const configCheckResult = pbiUtils.validateConfig({
        ...pbiUtils.config,
        workspaceId,
        reportId,
    });
    if (configCheckResult) {
        return res.status(400).json(configCheckResult);
    }

    try {
        // Get the details like Embed URL, Access token and Expiry
        let result = await pbiEmbedToken.getEmbedInfo(workspaceId, reportId);

        // result.status specified the statusCode that will be sent along with the result object
        res.status(result.status || 500).send(result);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    list,
    create,
    adminList,
    update,
    adminDetail,
    delete: deleteMetadata,
    uploadThumbnail,
    deleteThumbnail,
    thumbnail,
    trackViews,
    getPowerBIToken,
};
