const { body, param, query } = require("express-validator/check");
const errorMessages = require("../../modules/error/messages");
const { hasValidTagIds } = require("../../modules/validation");
const { hasValidMetadataId } = require("../../modules/validation");

const create = [
    body("route").exists().withMessage(errorMessages.required),
    body("client_route").exists().withMessage(errorMessages.required),
    body("type").exists().withMessage(errorMessages.required),
    body("component").exists().withMessage(errorMessages.required),
    body("fa_icon").optional(),
    body("title").exists().withMessage(errorMessages.required),
    body("description").exists().withMessage(errorMessages.required),
    body("data").optional(),
    body("tags")
        .exists()
        .withMessage(errorMessages.required)
        .custom(hasValidTagIds)
        .withMessage(errorMessages.not_found),
];

const checkId = () => {
    return param("metadataId")
        .exists()
        .withMessage(errorMessages.required)
        .isUUID()
        .withMessage(errorMessages.not_an_id);
};

const adminDetail = [checkId()];

const listMetadata = [query("sortBy").customSanitizer(value => value ?? "id").isString().isIn([ "id", "visits" ])];

const deleteMetadata = [checkId()];

const getPowerBIToken = [checkId()];

const checkMetadataId = [
    param("metadataId").exists().withMessage(errorMessages.required),
    param("metadataId").isUUID().withMessage(errorMessages.not_an_id),
    param("metadataId").custom(hasValidMetadataId).withMessage(errorMessages.not_found),
];

module.exports = {
    create,
    adminDetail,
    update: [...create, checkId()],
    list: listMetadata,
    delete: deleteMetadata,
    checkMetadataId,
    getPowerBIToken,
};
