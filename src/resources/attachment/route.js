const { Router } = require("express");
const { configLoader } = require("../accessrequest/configLoader");
const policy = require("../../modules/policy");
const forms = require("./forms");
const controller = require("./controller");
const checkErrors = require("../../modules/validation").checkErrors;
const router = new Router();
const multer = require("multer");
const storage = multer.memoryStorage();
const upload = multer({ storage, limits: { fieldSize: configLoader.storage.fileMaxSize, fileSize: configLoader.storage.fileMaxSize } });
const baseUrl = "/metadata/:metadataId/attachment";

router.put(
    baseUrl,
    policy.isSignedIn,
    policy.isAdmin,
    upload.single("attachmentFile"),
    forms.fileToBody,
    forms.update,
    checkErrors,
    controller.update
);

router.delete(baseUrl, policy.isSignedIn, policy.isAdmin, forms.del, checkErrors, controller.delete);

router.get(baseUrl, policy.isSignedIn, forms.get, checkErrors, controller.detail);

module.exports = router;
