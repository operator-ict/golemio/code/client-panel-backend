const { body, param } = require("express-validator/check");
const errorMessages = require("../../modules/error/messages");
const { hasValidMetadataId } = require("../../modules/validation");

// This allows validation through req.body
const fileToBody = (req, res, next) => {
    if (req.file) req.body.attachmentFile = req.file.originalname;
    next();
};

const update = [
    body("attachmentFile").exists().withMessage(errorMessages.required),
    body("attachmentTitle").exists().withMessage(errorMessages.required),
    param("metadataId").exists().withMessage(errorMessages.required),
    param("metadataId").isUUID().withMessage(errorMessages.not_an_id),
    param("metadataId").custom(hasValidMetadataId).withMessage(errorMessages.not_found),
];

const del = [
    param("metadataId")
        .exists()
        .withMessage(errorMessages.required)
        .custom(hasValidMetadataId)
        .withMessage(errorMessages.not_found),
];

const get = [
    param("metadataId")
        .exists()
        .withMessage(errorMessages.required)
        .custom(hasValidMetadataId)
        .withMessage(errorMessages.not_found),
];

module.exports = {
    fileToBody,
    update,
    del,
    get,
};
