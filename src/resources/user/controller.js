const PostgresConnector = require("../../dataAccess/postgresConnector");

const detail = (req, res, next) => {
    PostgresConnector.getModel("user")
        .findOne({ where: { user_id: req.session.user.id }, include: "favourite-tiles" })
        .then((user) => res.json(user ? { favouriteTiles: user["favourite-tiles"].map((item) => item.metadata_id) } : {}))
        .catch(next);
};

const update = async (req, res, next) => {
    try {
        const newFavouriteTiles = req.body.favouriteTiles; //array of metadataIds
        const [user, isNew] = await PostgresConnector.getModel("user").findOrCreate({
            where: { user_id: req.session.user.id },
            include: "favourite-tiles",
            default: {
                user_id: req.session.user.id,
                "favourite-tiles": [],
            },
        });

        const favouriteTilesToDelete = user["favourite-tiles"]
            ? user["favourite-tiles"].filter((item) => !newFavouriteTiles.includes(item.metadata_id))
            : [];
        const favouriteTilesToCreate = user["favourite-tiles"]
            ? newFavouriteTiles.filter((item) => !user["favourite-tiles"].map((item) => item.metadata_id).includes(item))
            : newFavouriteTiles;

        if (favouriteTilesToCreate.length + favouriteTilesToDelete.length > 0) {
            const t = await PostgresConnector.getConnection().transaction();
            try {
                for (const favouriteTileDao of favouriteTilesToDelete) {
                    await favouriteTileDao.destroy({ transaction: t });
                }

                for (const favouriteTile of favouriteTilesToCreate) {
                    await PostgresConnector.getModel("favourite-tiles").create(
                        {
                            user_id: user.id,
                            metadata_id: favouriteTile,
                        },
                        { transaction: t }
                    );
                }
                await t.commit();
            } catch (err) {
                await t.rollback();
                getLogger("UserController").error(err);
                throw err;
            }
        }

        res.sendStatus(204);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    detail,
    update,
};
