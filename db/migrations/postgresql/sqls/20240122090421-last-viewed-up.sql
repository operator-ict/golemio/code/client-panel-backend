-- public."last_views" definition

CREATE TABLE "last_views" (
    "user_id" integer NOT NULL,
	"metadata_id" uuid NOT NULL,
    "last_visit" timestamptz NOT NULL DEFAULT now(),
    "number_of_visits_normalized" integer NOT NULL DEFAULT 1,
    "updated_at" timestamptz NOT NULL DEFAULT now(),
    "created_at" timestamptz NOT NULL DEFAULT now(),
    CONSTRAINT last_viewed_pk PRIMARY KEY ("user_id", "metadata_id"),
    CONSTRAINT "last_viewed_user_fk" FOREIGN KEY ("user_id") REFERENCES "user"("user_id") ON DELETE CASCADE,
    CONSTRAINT "last_viewed_metadata_fk" FOREIGN KEY ("metadata_id") REFERENCES metadata("id") ON DELETE CASCADE
);
