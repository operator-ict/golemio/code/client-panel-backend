CREATE OR REPLACE VIEW v_metadata_golemio_bi as 
SELECT 
    type,
    component,
    route,
    client_route, 
    title, 
    description 
FROM metadata;