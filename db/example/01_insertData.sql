--0e37df36-f698-11e6-8dd4-cb9ced3df976
--5fdb5ae2-b81d-9090-66fa-216700000000
--00000000-5fdb-5ae2-b81d-909066fa2167
INSERT INTO tag (id, title) values
('000000005fdb5ae2b81d909066fa2167','Doprava'),
('000000005fdb5b0bb81d909066fa2168','COVID-19'),
('000000005fdb5b25b81d909066fa2169','Bydlení'),
('000000006011c128c2af490013b81e7b','Odpady'),
('000000006011c39ec2af490013b81e7c','Parkování'),
('0000000060128f05c2af490013b81e7e','MHD'),
('000000006013da1e180d0e00122d05ad','PID Lítačka'),
('000000006013e304181493001340dbe8','Správa majetku'),
('000000006013e427181493001340dbe9','Reporting MHMP'),
('000000006013e4d1181493001340dbea','Životí prostředí'),
('000000006013e551181493001340dbeb','Reporting OICT'),
('000000006013e98c181493001340dbec','Cyklodoprava');

insert into	metadata (
    "id",
	"type",
	"component",
	"route",
	"client_route",
	"fa_icon",
	"title",
	"description",
	"data")
values(
'00000000601139b2a382a678ba6c85f3',
'application',
'sortedWasteMap',
'/v2/sortedwastestations',
'/sortedwastestations',
'',
'Mapa stanovišť na tříděný odpad',
'Obsahuje všechny stanoviště v Praze, lze filtrovat pouze nádoby osazené senzorem pro měření zaplněnosti.',
'{
    "districtsRoute" : "/v2/citydistricts",
    "measurementsRoute" : "/v2/sortedwastestations/measurements"
}');

insert into	"metadata-tag" ("metadata_id","tag_id")
values ('00000000601139b2a382a678ba6c85f3','000000005fdb5ae2b81d909066fa2167')
