# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.9] - 2024-04-02

### Changed

-   allow different image ratio for new FE

## [2.0.8] - 2024-02-12

### Fixed

-   hotfix of release 2.0.7

## [2.0.7] - 2024-02-07

### Removed

-   Remove Recaptcha ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [2.0.6] - 2024-02-05

### Added

-   Tracking dashboard visits ([frontend#48](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/48))

## [2.0.5] - 2024-01-21

### Changed

-   Improve README.md

### Removed

-   Apiary docs (will permanently move to OpenAPI) ([general#498](https://gitlab.com/operator-ict/golemio/code/general/-/issues/498))

## [2.0.4] - 2023-09-29

### Fixed

-   Fix PBI embed error handling

## [2.0.3] - 2023-05-23

### Added

-   Add cache for PBI embed authorization

### Changed

-   Migration to node 18

## [2.0.2] - 2023-05-15

### Changed

-   Replace ADAL with MSAL

## [2.0.1] - 2022-11-14

### Added

-   Extend azure blob storage service with delete and get function
-   Add view `v_metadata_golemio_bi`

### Fixed

-   Fix "Show tiles for user: ..."

## [2.0.0] - 2022-04-11

### Changed

-   migrated from Mongo DB to postgres and S3
