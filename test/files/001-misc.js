const init = require("../init");
const APIError = require("../../src/modules/error");
const Authentication = require("../../src/modules/authentication");
const postgresConnector = require("../../src/dataAccess/postgresConnector");

let agent;

describe("Misc", function() {
  this.timeout(100000);

  before(function(done) {
    init().then(object => {
      agent = object.agent;
      done();
    });
  });

  it("GET /api/nonexistingroute should return 404 route not found", function(done) {
    agent.get("/api/nonexistingroute").expect(404, function(err, res) {
      done(err);
    });
  });

  it("GET /status should return 200 app is running", function(done) {
    agent.get("/status").expect(200, function(err, res) {
      res.body.should.have.property("app_name", "client-panel-backend");
      res.body.should.have.property("status", "Up");
      done(err);
    });
  });

  it("should create API error with default status 500", done => {
    const error = new APIError("test");
    error.should.have.property("status", 500);
    done();
  });

  it("should fail to get model", done => {
    try {
      postgresConnector.getModel("invalid-model");
      done(new Error("should have thrown"));
    } catch (err) {
      done();
    }
  });

  it("should fail to init authentication", done => {
    try {
      new Authentication({ strategy: "invalid-strategy" });
      done(new Error("should have thrown"));
    } catch (err) {
      done();
    }
  });
});
