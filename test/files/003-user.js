const init = require("../init");
const postgresConnector = require("../../src/dataAccess/postgresConnector");

let agent;
let users;

describe("User", function () {
    this.timeout(100000);

    before(async () => {
        await init().then((object) => {
            agent = object.agent;
            users = object.users;
        });
        await postgresConnector.getModel("user").truncate({ cascade: true });
    });

    it("GET /user should return 200 nothing stored", function (done) {
        agent
            .get("/user")
            .set("x-access-token", users.user1.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Object).and.be.empty();
                done(err);
            });
    });

    it("PUT /user should return 204 set user settings", function (done) {
        agent
            .put("/user")
            .set("content-type", "application/json")
            .set("x-access-token", users.user1.token)
            .send(
                JSON.stringify({
                    favouriteTiles: ["00000000-5de1-308c-d13e-6410b804ac01"],
                })
            )
            .expect(204, function (err, res) {
                done(err);
            });
    });

    it("GET /user should return 200 user storage", function (done) {
        agent
            .get("/user")
            .set("x-access-token", users.user1.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Object);
                res.body.should.have.property("favouriteTiles");
                res.body.favouriteTiles[0].should.equal("00000000-5de1-308c-d13e-6410b804ac01");
                done(err);
            });
    });
});
