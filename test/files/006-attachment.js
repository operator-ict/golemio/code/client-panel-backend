const sinon = require("sinon");
const init = require("../init");
const pbiUtils = require("../../src/modules/powerbiembed/utils");
const pbiEmbedToken = require("../../src/modules/powerbiembed/embedConfigService");
const postgresConnector = require("../../src/dataAccess/postgresConnector");
const storageService = require("../../src/dataAccess/storageService");

let agent;
let users;
const METADATA_ID = "00000000-5de1-308c-d13e-6410b804ac01";
const S3_KEY = `golemiobi-backend-test/thumbnails/${METADATA_ID}`;
const WRONG_METADATA_ID = "605975818391d209d085605d";
const ATTACHMENT_ID = "000000006059221c694b8446e80481e9";
const TAG_ID_1 = "000000005fdb5ae2b81d909066fa2167";
const TAG_ID_2 = "000000005fdb5b0bb81d909066fa2168";
const TAG_ID_3 = "000000005fdb5b25b81d909066fa2169";

describe("Attachment", function () {
    this.timeout(100000);

    let sandbox;

    before((done) => {


        init()
            .then((object) => {
                agent = object.agent;
                users = object.users;
            })
            .then(() => done())
            .catch(done);
    });

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(storageService, "uploadAttachment").callsFake(() => null);
        sandbox.stub(storageService, "deleteObjectByKey").callsFake(() => null);
        await postgresConnector.getModel("metadata").truncate({ cascade: true });
        await postgresConnector.getModel("tag").truncate({ cascade: true });
        await postgresConnector.getModel("tag").bulkCreate([
            {
                id: TAG_ID_1,
                title: "Test",
            },
            {
                id: TAG_ID_2,
                title: "Test 2",
            },
            {
                id: TAG_ID_3,
                title: "Test 3",
            },
        ]);
        await postgresConnector.getModel("metadata").create(
            {
                id: METADATA_ID,
                type: "map",
                component: "mapComponent",
                //route from dummy strategy
                route: "/parkings",
                client_route: "/parkings",
                title: "Nadpis 1",
                description: "Popis 1",
                metadataTags: [{ tag_id: TAG_ID_1 }, { tag_id: TAG_ID_2 }],
                thumbnail_url: `/metadata/${METADATA_ID}/thumbnail`,
                thumbnail_s3_key: S3_KEY,
                thumbnail_content_type: "image/png",
            },
            { include: "metadataTags" }
        );
        await postgresConnector.getModel("attachments").create({
            id: ATTACHMENT_ID,
            title: "Attachment ttle",
            metadata_id: METADATA_ID,
            url: "",
        });
        await postgresConnector.getModel("metadata").create(
            {
                type: "map",
                component: "mapComponent",
                route: "/not-a-route",
                client_route: "/",
                title: "Nadpis 2",
                description: "Popis 2",
                metadataTags: [{ tag_id: TAG_ID_3 }],
            },
            { include: "metadataTags" }
        );

        sandbox.stub(pbiUtils, "validateConfig").callsFake(() => null);
        sandbox.stub(pbiEmbedToken, "getEmbedInfo").callsFake(() => ({
            accessToken: "ok",
            expiry: new Date().toISOString(),
            status: 200,
            embedUrl: [],
        }));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("PUT  /metadata/nonsense/attachment should return 400", (done) => {
        agent
            .put(`/metadata/nonsense/attachment`)
            .set("x-access-token", users.user2.token)
            .expect(400, (err, res) => {
                done(err);
            });
    });

    it(`PUT /metadata/:metadataId/attachment should return 403 not allowed for non admin`, function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/attachment`)
            .set("x-access-token", users.user1.token)
            .set("content-type", "multipart/form-data")
            .send(JSON.stringify({}))
            .expect(403, function (err, res) {
                done(err);
            });
    });

    it(`PUT /metadata/:metadata/attachment should return 200`, function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "multipart/form-data")
            .field("attachmentTitle", "Title text")
            .attach("attachmentFile", __dirname + "/../testimage.png")
            .expect(200, function (err, res) {
                done(err);
            });
    });

    it(`PUT /metadata/:wrongId/attachment should return 400`, function (done) {
        agent
            .put(`/metadata/${WRONG_METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "multipart/form-data")
            .field("attachmentTitle", "Title text")
            .attach("attachmentFile", __dirname + "/../testimage.png")
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it(`PUT /metadata/:metadata/attachment should return 413 (file limit exceeded)`, function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "multipart/form-data")
            .field("attachmentTitle", "Title text")
            .attach("attachmentFile", __dirname + "/../testtoobig.jpg")
            .expect(413, function (err, res) {
                done(err);
            });
    });

    it(`DELETE /metadata/:metadataId/attachment should return 200`, function (done) {
        agent
            .delete(`/metadata/${METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .expect(200, function (err, res) {
                done(err);
            });
    });

    it(`DELETE /metadata/:metadataId/attachment should return 400`, function (done) {
        agent
            .delete(`/metadata/${WRONG_METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it(`GET /metadata/:wrongId/attachment should return 400`, (done) => {
        agent
            .get(`/metadata/${WRONG_METADATA_ID}/attachment`)
            .set("x-access-token", users.user2.token)
            .expect(400, (err, res) => {
                done(err);
            });
    });
});
