const sinon = require("sinon");
const fs = require("fs");
const init = require("../init");
const pbiUtils = require("../../src/modules/powerbiembed/utils");
const pbiEmbedToken = require("../../src/modules/powerbiembed/embedConfigService");
const postgresConnector = require("../../src/dataAccess/postgresConnector");
const storageService = require("../../src/dataAccess/storageService");
const rp = require("request-promise");

let agent;
let users;

const METADATA_ID = "00000000-5de1-308c-d13e-6410b804ac01";
const STORAGE_KEY = `golemiobi-backend-test/thumbnails/${METADATA_ID}`;
const WRONG_METADATA_ID = "605c9eb8526f0f3060e47574";
const TAG_ID_1 = "00000000-5fdb-5ae2-b81d-909066fa2167";
const TAG_ID_2 = "00000000-5fdb-5b0b-b81d-909066fa2168";
const TAG_ID_3 = "00000000-5fdb-5b25-b81d-909066fa2169";

describe("Metadata", function () {
    this.timeout(100000);

    let sandbox;

    before((done) => {
        init()
            .then((object) => {
                agent = object.agent;
                users = object.users;
            })
            .then(() => done())
            .catch(done);
    });

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        sandbox.stub(storageService, "uploadThumbnail").returns("storage-key");
        sandbox.stub(storageService, "deleteObjectByKey").callsFake(() => null);
        sandbox.stub(storageService, "getObjectAsStream").returns({
            pipe: sinon.stub().callsFake((res) => res.end()),
        });

        sandbox.stub(rp, "get").returns(
            Promise.resolve([
                {
                    user_id: 2,
                    endpoint: "/parkings",
                    method: "GET",
                },
            ])
        );

        await postgresConnector.getModel("metadata").truncate({ cascade: true });
        await postgresConnector.getModel("tag").truncate({ cascade: true });
        await postgresConnector.getModel("tag").bulkCreate([
            {
                id: TAG_ID_1,
                title: "Test",
            },
            {
                id: TAG_ID_2,
                title: "Test 2",
            },
            {
                id: TAG_ID_3,
                title: "Test 3",
            },
        ]);
        await postgresConnector.getModel("metadata").create(
            {
                id: METADATA_ID,
                type: "map",
                component: "mapComponent",
                //route from dummy strategy
                route: "/parkings",
                client_route: "/parkings",
                title: "Nadpis 1",
                description: "Popis 1",
                metadataTags: [{ tag_id: TAG_ID_1 }, { tag_id: TAG_ID_2 }],
                thumbnail_url: `/metadata/${METADATA_ID}/thumbnail`,
                thumbnail_s3_key: STORAGE_KEY,
                thumbnail_content_type: "image/png",
            },
            { include: "metadataTags" }
        );
        await storageService.uploadThumbnail(fs.readFileSync(__dirname + "/../testimage.png"), METADATA_ID, "test-image.png");
        await postgresConnector.getModel("metadata").create(
            {
                type: "map",
                component: "mapComponent",
                route: "/not-a-route",
                client_route: "/",
                title: "Nadpis 2",
                description: "Popis 2",
                metadataTags: [{ tag_id: TAG_ID_3 }],
            },

            { include: "metadataTags" }
        );

        sandbox.stub(pbiUtils, "validateConfig").callsFake(() => null);
        sandbox.stub(pbiEmbedToken, "getEmbedInfo").callsFake(() => ({
            accessToken: "ok",
            expiry: new Date().toISOString(),
            status: 200,
            embedUrl: [],
        }));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("GET /user/1/metadata should return 400 no access token present", function (done) {
        agent.get("/user/1/metadata").expect(400, function (err, res) {
            done(err);
        });
    });

    it("GET /user/1/metadata should return 401 unauthorized", function (done) {
        agent
            .get("/user/1/metadata")
            .set("x-access-token", "tttttt")
            .expect(401, function (err, res) {
                done(err);
            });
    });

    it("GET /user/2/metadata should return 403 no access to foreign metadata", function (done) {
        agent
            .get("/user/2/metadata")
            .set("x-access-token", users.user1.token)
            .expect(403, function (err, res) {
                done(err);
            });
    });

    it("GET /user/1/metadata should return 200 with empty array", function (done) {
        agent
            .get("/user/1/metadata")
            .set("x-access-token", users.user1.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(0);
                done(err);
            });
    });

    it("GET /user/2/metadata should return 200", function (done) {
        agent
            .get("/user/2/metadata")
            .set("x-access-token", users.user2.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(1);
                res.body[0].should.have.property("type", "map");
                res.body[0].should.have.property("component", "mapComponent");
                res.body[0].should.have.property("route", "/parkings");
                res.body[0].should.have.property("client_route", "/parkings");
                res.body[0].should.have.property("title", "Nadpis 1");
                res.body[0].should.have.property("description", "Popis 1");
                res.body[0].should.have.property("tags");
                res.body[0].tags[0].should.have.property("title", "Test");
                res.body[0].tags[0].should.have.property("_id", TAG_ID_1);
                done(err);
            });
    });

    it("GET /user/2/metadata as admin should return 200", function (done) {
        agent
            .get("/user/2/metadata")
            .set("x-access-token", users.user3.token)
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(1);
                res.body[0].should.have.property("type", "map");
                res.body[0].should.have.property("component", "mapComponent");
                res.body[0].should.have.property("route", "/parkings");
                res.body[0].should.have.property("client_route", "/parkings");
                res.body[0].should.have.property("title", "Nadpis 1");
                res.body[0].should.have.property("description", "Popis 1");
                res.body[0].should.have.property("tags");
                res.body[0].tags[0].should.have.property("_id");
                res.body[0].tags[0].should.have.property("title");
                res.body[0].tags[0].title.should.be.oneOf(["Test", "Test 2"]);
                res.body[0].tags[0]._id.should.be.oneOf([TAG_ID_1, TAG_ID_2]);
                done(err);
            });
    });

    it("POST /metadata should return 403 not allowed for non admin", function (done) {
        agent
            .post("/metadata")
            .set("x-access-token", users.user1.token)
            .set("content-type", "application/json")
            .send(JSON.stringify({}))
            .expect(403, function (err, res) {
                done(err);
            });
    });

    it("POST /metadata should return 201 create new metadata", function (done) {
        agent
            .post("/metadata")
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    route: "/dashboard/2",
                    client_route: "/analysis/transportation",
                    component: "component",
                    type: "powerbi",
                    title: "Analýza jízdních řádů",
                    description: "Přehled počtu linek a spojů během dne",
                    data: {
                        url: "powerbi link",
                    },
                    tags: [TAG_ID_1],
                })
            )
            .expect(201, function (err, res) {
                done(err);
            });
    });

    it("POST /metadata should return 409 on this route already exist", function (done) {
        agent
            .post("/metadata")
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    type: "map",
                    component: "mapComponent",
                    route: "/parkings",
                    client_route: "/parkings",
                    title: "Nadpis 1",
                    description: "Popis 1",
                    tags: [TAG_ID_2],
                })
            )
            .expect(409, function (err, res) {
                done(err);
            });
    });

    it("GET /metadata should return all metadata settings", function (done) {
        agent
            .get("/metadata")
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .expect(200, function (err, res) {
                res.body.should.be.instanceOf(Array).and.lengthOf(2);
                const meta1 = res.body[0];
                const meta2 = res.body[1];
                meta1.should.have.property("title", "Nadpis 1");
                meta2.should.have.property("title", "Nadpis 2");
                done(err);
            });
    });

    it("GET /metadata/{id} should return metadata detail", function (done) {
        agent
            .get(`/metadata/${METADATA_ID}`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .expect(200, function (err, res) {
                res.body.should.have.property("_id", METADATA_ID);
                res.body.should.have.property("route", "/parkings");
                done(err);
            });
    });

    it("PUT /metadata/{metadataId} should return 204 should update metadata", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .send(
                JSON.stringify({
                    route: "/dashboard/1",
                    client_route: "/dashboard/1",
                    type: "dashboard",
                    component: "component",
                    title: "Analýza jízdních řádů",
                    description: "Přehled počtu linek a spojů během dne",
                    tags: [TAG_ID_1, TAG_ID_2],
                })
            )
            .expect(204, function (err, res) {
                done(err);
            });
    });

    it("GET /user/1/metadata should get user's metadata", async () => {
        await postgresConnector.getModel("metadata").create({
            route: "/lights",
            client_route: "/dashboard/1",
            type: "dashboard",
            component: "component",
            title: "Analýza jízdních řádů",
            description: "Přehled počtu linek a spojů během dne",
        });

        const getRes = await agent.get("/user/2/metadata").set("x-access-token", users.user2.token);

        getRes.statusCode.should.equal(200);
        getRes.body.should.be.instanceOf(Array).and.lengthOf(2);
    });

    it("DELETE /metadata/{metadataId} should return 204 should delete metadata", function (done) {
        agent
            .delete(`/metadata/${METADATA_ID}`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .send()
            .expect(204, function (err, res) {
                postgresConnector
                    .getModel("metadata")
                    .findAll({ where: { id: METADATA_ID } })
                    .then((metadata) => {
                        metadata.should.be.instanceOf(Array).and.lengthOf(0);
                        done(err);
                    })
                    .catch(done);
            });
    });

    it("GET /metadata/nonsense should return 400", (done) => {
        agent
            .get(`/metadata/nonsense`)
            .set("x-access-token", users.user2.token)
            .expect(400, (err, res) => {
                done(err);
            });
    });

    it("GET /metadata/:wrongId should return 404", (done) => {
        const wrongId = [6, ...METADATA_ID.substring(1)].join("");
        agent
            .get(`/metadata/${wrongId}`)
            .set("x-access-token", users.user2.token)
            .expect(404, (err, res) => {
                done(err);
            });
    });

    it("PUT /metadata/{metadataId}/thumbnail should return 201 should upload metadata thumbnail", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .attach("file", __dirname + "/../testimage.png")
            .expect(201, function (err, res) {
                done(err);
            });
    });

    it("GET /metadata/{id}/thumbnail should return metadata thumbnail image", function (done) {
        agent
            .get(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .set("content-type", "application/json")
            .expect(200, function (err, res) {
                res.headers["content-type"].should.equal("image/png");
                done(err);
            });
    });

    it("PUT /metadata/{metadataId}/thumbnail should return 400 (no file)", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            // .attach("thumb", __dirname + "/../testimage.png")
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it("PUT /metadata/{metadataId}/thumbnail should return 400 (small image)", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .attach("file", __dirname + "/../testsmall.png")
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it("PUT /metadata/{metadataId}/thumbnail should return 400 (bad ratio)", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .attach("file", __dirname + "/../testbadratio.png")
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it("PUT /metadata/{metadataId}/thumbnail should return 400 (bad format)", function (done) {
        agent
            .put(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .attach("file", __dirname + "/../testtext.txt")
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it("DEL /metadata/{metadataId}/thumbnail should return 200 should delete metadata thumbnail", function (done) {
        agent
            .delete(`/metadata/${METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .expect(200, function (err, res) {
                done(err);
            });
    });

    it("DEL /metadata/{wrong_metadataId}/thumbnail should return 400", function (done) {
        agent
            .delete(`/metadata/${WRONG_METADATA_ID}/thumbnail`)
            .set("x-access-token", users.user2.token)
            .expect(400, function (err, res) {
                done(err);
            });
    });

    it("GET /metadata/:id/powerbi-embed-token should return 403 no access to foreign metadata", function (done) {
        agent.get(`/metadata/${METADATA_ID}/powerbi-embed-token`).set("x-access-token", users.user1.token).expect(403, done);
    });

    it("GET /metadata/:id/powerbi-embed-token should return 200", function (done) {
        agent
            .get(`/metadata/${METADATA_ID}/powerbi-embed-token`)
            .set("x-access-token", users.user2.token)
            .expect(200, function (err, res) {
                res.body.should.have.property("accessToken", "ok");
                res.body.should.have.property("expiry");
                res.body.should.have.property("status", 200);
                res.body.should.have.property("embedUrl");
                done(err);
            });
    });
});
