const request = require("supertest");
const app = require("../src/app");

const object = {};

module.exports = async () => {
  if (process.env.NODE_ENV !== "test") {
    throw new Error('NODE_ENV must be "test" when running tests');
  }

  await app.start();

  object.agent = request.agent(app.getExpress());

  object.users = {
    user1: { name: "User 1", token: "token1" },
    user2: { name: "User 2", token: "token2" },
    user3: { name: "User 3", token: "token3" },
  };

  return object;
};
